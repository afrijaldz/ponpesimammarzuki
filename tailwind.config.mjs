/** @type {import('tailwindcss').Config} */
export default {
  content: ["./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}"],
  theme: {
    extend: {
      colors: {
        "i-main": "#004851",
        "i-secondary": "#054D22",
      },
    },
  },
  plugins: [],
};
